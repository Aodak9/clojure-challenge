(ns thread-last-operator.clj)

(defn invoice-items-by-conditions [invoice]
      (->> invoice
           :invoice/items
           (filter (fn [item] (or (some #(= :iva (:tax/category %)) (:invoice-item/taxes item))
                                  (some #(= :ret_fuente (:retentions/category %)) (:invoice-item/retentions item)))))))

