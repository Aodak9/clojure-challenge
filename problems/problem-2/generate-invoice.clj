(ns generate-invoice.clj
    (:require [clojure.data.json :as json]
      [clojure.edn :as edn]))

(defn invoice-from-json [file-name]
      (let [data (json/read-str (slurp file-name))]
           (-> data :invoice
               (edn/read-string))))
