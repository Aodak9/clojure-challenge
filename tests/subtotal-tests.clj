(ns subtotal-tests.clj
    (:require [clojure.test :refer [deftest is testing]]
      [subtotal-tests.clj :refer [subtotal]]))

(deftest test-subtotal
         (testing "subtotal with no discount"
                  (is (= 100.0 (subtotal {:invoice-item/quantity 1
                                          :invoice-item/price 100.0})))
                  (is (= 200.0 (subtotal {:invoice-item/quantity 2
                                          :invoice-item/price 100.0}))))
         (testing "subtotal with discount"
                  (is (= 90.0 (subtotal {:invoice-item/quantity 1
                                         :invoice-item/price 100.0
                                         :invoice-item/discount-rate 10})))
                  (is (= 170.0 (subtotal {:invoice-item/quantity 3
                                          :invoice-item/price 100.0
                                          :invoice-item/discount-rate 15}))))
         (testing "subtotal with precision"
                  (is (= 100.5 (subtotal {:invoice-item/quantity 1.01
                                          :invoice-item/price 100.5})))
                  (is (= 200.5 (subtotal {:invoice-item/quantity 2.01
                                          :invoice-item/price 100.5})))))
